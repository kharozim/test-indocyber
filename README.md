## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/kharozim/test-indocyber.git
git branch -M main
git push -uf origin main
```

## FITUR FITUR


### Genre

- Fitur Menampilkan List Genre
- FItur Pencarian Genre
- Fitur Loading State
- Fitur Refresh ketika Error & Empty

### Movie by GenreId

- Fitur Menampilkan List Pagination Movie
- FItur Loading State
- Fitur Refresh ketika Error & Empty
- Fitur Retry (melanjutkan request) ketika koneksi berhenti ditengah scroll

### Detail Movie

- Fitur Menampilkan data detail movie
- Fitur Menampilkan list video yang tersedia
- Fitur Menjalankan video dari youtube
- Fitur Menampilkan List Review yang tersedia

### Review

- Fitur Menampilkan List Pagination Review
- FItur Loading State
- Fitur Refresh ketika Error & Empty
- Fitur Retry (melanjutkan request) ketika koneksi berhenti ditengah scroll