package com.ozimos.test.data

import androidx.paging.PagingData
import com.ozimos.test.data.remote.StateResponse
import com.ozimos.test.domain.model.*
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    suspend fun getListGenre(): StateResponse<GenreModel>
    suspend fun getMovieByGenrePaging3(genreIds: String): Flow<PagingData<MovieModel>>
    suspend fun getDetailMovie(movieId: Int): StateResponse<DetailMovieModel>
    suspend fun getListMovieVideo(movieId: Int): StateResponse<List<VideoItemItemModel>>
    suspend fun getListMovieReviewsPaging3(
        movieId: Int,
        isNextPage: Boolean
    ): Flow<PagingData<ReviewModel>>
}
