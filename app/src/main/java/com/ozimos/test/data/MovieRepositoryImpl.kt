package com.ozimos.test.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.ozimos.test.data.pagingsource.MoviePagingSource
import com.ozimos.test.data.pagingsource.ReviewPagingSource
import com.ozimos.test.data.remote.ApiService
import com.ozimos.test.data.remote.ErrorUtil
import com.ozimos.test.data.remote.StateResponse
import com.ozimos.test.domain.model.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(private val service: ApiService) : MovieRepository {
    override suspend fun getListGenre(): StateResponse<GenreModel> {
        return try {
            val response = service.getListGenre()
            if (response.isSuccessful) {
                val data = response.body()?.toModel() ?: GenreModel()
                StateResponse.Success(data = data)
            } else {
                val message = ErrorUtil.getErrorMessage(response.errorBody()?.string() ?: "")
                StateResponse.Failed(message)
            }

        } catch (e: Exception) {
            StateResponse.Failed(e.localizedMessage ?: "Error Exception")
        }
    }

    override suspend fun getMovieByGenrePaging3(genreIds: String): Flow<PagingData<MovieModel>> {
        return Pager(
            config = PagingConfig(pageSize = 5, prefetchDistance = 1),
            pagingSourceFactory = { MoviePagingSource(service, genreIds) }
        ).flow
    }

    override suspend fun getDetailMovie(movieId: Int): StateResponse<DetailMovieModel> {
        return try {
            val response = service.getDetailMovie(movieId)
            if (response.isSuccessful) {
                val data = response.body()?.toModel() ?: DetailMovieModel()
                StateResponse.Success(data = data)
            } else {
                val message = ErrorUtil.getErrorMessage(response.errorBody()?.string() ?: "")
                StateResponse.Failed(message)
            }

        } catch (e: Exception) {
            StateResponse.Failed(e.localizedMessage ?: "Error Exception")
        }
    }

    override suspend fun getListMovieVideo(movieId: Int): StateResponse<List<VideoItemItemModel>> {
        return try {
            val response = service.getListMovieVideo(movieId)
            if (response.isSuccessful) {
                val data = response.body()?.toModel()?.results ?: emptyList()
                StateResponse.Success(data = data)
            } else {
                val message = ErrorUtil.getErrorMessage(response.errorBody()?.string() ?: "")
                StateResponse.Failed(message)
            }

        } catch (e: Exception) {
            StateResponse.Failed(e.localizedMessage ?: "Error Exception")
        }
    }

    override suspend fun getListMovieReviewsPaging3(
        movieId: Int,
        isNextPage: Boolean
    ): Flow<PagingData<ReviewModel>> {
        return Pager(
            config = PagingConfig(pageSize = 5, prefetchDistance = 1),
            pagingSourceFactory = { ReviewPagingSource(service, movieId, isNextPage) }
        ).flow
    }
}
