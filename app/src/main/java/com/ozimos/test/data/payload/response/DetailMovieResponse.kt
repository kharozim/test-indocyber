package com.ozimos.test.data.payload.response

import com.google.gson.annotations.SerializedName
import com.ozimos.test.domain.model.DetailMovieModel

data class DetailMovieResponse(

    @field:SerializedName("original_language")
    val originalLanguage: String? = null,

    @field:SerializedName("imdb_id")
    val imdbId: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("backdrop_path")
    val backdropPath: String? = null,

    @field:SerializedName("revenue")
    val revenue: Int? = null,


    @field:SerializedName("popularity")
    val popularity: Double? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("vote_count")
    val voteCount: Int? = null,

    @field:SerializedName("budget")
    val budget: Int? = null,

    @field:SerializedName("original_title")
    val originalTitle: String? = null,

    @field:SerializedName("runtime")
    val runtime: Int? = null,

    @field:SerializedName("poster_path")
    val posterPath: String? = null,

    @field:SerializedName("release_date")
    val releaseDate: String? = null,

    @field:SerializedName("vote_average")
    val voteAverage: Double? = null,

    @field:SerializedName("tagline")
    val tagline: String? = null,

    @field:SerializedName("adult")
    val adult: Boolean? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("genres")
    val genres: List<GenreItemResponse>? = null,

    @field:SerializedName("overview")
    val overview: String? = null,
) {
    fun toModel() = DetailMovieModel(
        originalLanguage = originalLanguage ?: "",
        imdbId = imdbId ?: "",
        title = title ?: "",
        backdropPath = backdropPath ?: "",
        revenue = revenue ?: 0,
        popularity = popularity ?: 0.0,
        id = id ?: 0,
        voteCount = voteCount ?: 0,
        budget = budget ?: 0,
        originalTitle = originalTitle ?: "",
        runtime = runtime ?: 0,
        posterPath = posterPath ?: "",
        releaseDate = releaseDate ?: "",
        voteAverage = voteAverage ?: 0.0,
        tagline = tagline ?: "",
        adult = adult ?: false,
        status = status ?: "",
        genres = genres?.asSequence()?.map { it.toModel() }?.toList() ?: emptyList(),
        overview = overview ?: "",
    )
}





