package com.ozimos.test.data.payload.response

import com.google.gson.annotations.SerializedName
import com.ozimos.test.domain.model.MovieModel


data class MovieResponse(

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("overview")
    val overview: String? = null,

    @field:SerializedName("original_language")
    val originalLanguage: String? = null,

    @field:SerializedName("original_title")
    val originalTitle: String? = null,

    @field:SerializedName("video")
    val video: Boolean? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("genre_ids")
    val genreIds: List<Int>? = null,

    @field:SerializedName("poster_path")
    val posterPath: String? = null,

    @field:SerializedName("backdrop_path")
    val backdropPath: String? = null,

    @field:SerializedName("release_date")
    val releaseDate: String? = null,

    @field:SerializedName("popularity")
    val popularity: Double? = null,

    @field:SerializedName("vote_average")
    val voteAverage: Double? = null,

    @field:SerializedName("adult")
    val adult: Boolean? = null,

    @field:SerializedName("vote_count")
    val voteCount: Int? = null
) {
    fun toModel(): MovieModel = MovieModel(
        id = id ?: "0",
        overview = overview ?: "",
        originalLanguage = originalLanguage ?: "",
        originalTitle = originalTitle ?: "",
        video = video ?: false,
        title = title ?: "",
        genreIds = genreIds ?: emptyList(),
        posterPath = posterPath ?: "",
        backdropPath = backdropPath ?: "",
        releaseDate = releaseDate ?: "",
        popularity = popularity ?: 0.0,
        voteAverage = voteAverage ?: 0.0,
        adult = adult ?: false,
        voteCount = voteCount ?: 0
    )
}


