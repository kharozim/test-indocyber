package com.ozimos.test.data.remote

import com.ozimos.test.data.payload.response.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {
    @GET("genre/movie/list")
    suspend fun getListGenre(): Response<GenreResponse>

    @GET("discover/movie")
    suspend fun getMovieByGenre(
        @Query("with_genres") genreIds: String,
        @Query("page") page: Int
    ): Response<PagingResponse<List<MovieResponse>>>

    @GET("movie/{movieId}")
    suspend fun getDetailMovie(
        @Path("movieId") movieId: Int,
    ): Response<DetailMovieResponse>

    @GET("movie/{movieId}/videos")
    suspend fun getListMovieVideo(
        @Path("movieId") movieId: Int,
    ): Response<VideoResponse>

    @GET("movie/{movieId}/reviews")
    suspend fun getListMovieReview(
        @Path("movieId") movieId: Int,
        @Query("page") page: Int,
    ): Response<PagingResponse<List<ReviewResponse>>>

}






