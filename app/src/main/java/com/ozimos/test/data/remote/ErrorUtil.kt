package com.ozimos.test.data.remote

import org.json.JSONObject

object ErrorUtil {
    fun getErrorMessage(errorResponse: String): String {
        return try {
            val responseError = JSONObject(errorResponse)
            val errorMessage = responseError.get("error")
            JSONObject(errorMessage.toString()).get("errors").toString()
        } catch (e: Exception) {
            "Error Not found"
        }

    }
}