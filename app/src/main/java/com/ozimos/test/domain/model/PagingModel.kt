package com.ozimos.test.domain.model

data class PagingModel(
    val page: Int = 0,
    val totalPages: Int = 0,
    val results: Any? = null,
    val totalResults: Int = 0
)