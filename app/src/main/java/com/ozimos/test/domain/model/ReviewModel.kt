package com.ozimos.test.domain.model

import com.ozimos.test.BuildConfig

data class ReviewModel(
    val authorDetails: AuthorDetailModel = AuthorDetailModel(),
    val updatedAt: String = "",
    val author: String = "",
    val createdAt: String = "",
    override val id: String = "",
    val content: String = "",
    val url: String = ""
) : BaseModel()

data class AuthorDetailModel(

    val avatarPath: String = "",
    val name: String = "",
    val rating: Double = 0.0,
    val username: String = ""
) {
    val avatarPathClean = BuildConfig.BASE_URL_IMAGE + avatarPath
}
