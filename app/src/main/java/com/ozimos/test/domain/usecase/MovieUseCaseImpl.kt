package com.ozimos.test.domain.usecase

import androidx.paging.PagingData
import com.ozimos.test.data.MovieRepository
import com.ozimos.test.data.remote.StateResponse
import com.ozimos.test.domain.model.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MovieUseCaseImpl @Inject constructor(private val repository: MovieRepository) :
    MovieUseCase {
    override suspend fun getListGenre(): StateResponse<GenreModel> {
        return repository.getListGenre()
    }

    override suspend fun getMovieByGenrePaging3(genreIds: String): Flow<PagingData<MovieModel>> {
        return repository.getMovieByGenrePaging3(genreIds)
    }

    override suspend fun getDetailMovie(movieId: Int): StateResponse<DetailMovieModel> {
        return repository.getDetailMovie(movieId)
    }

    override suspend fun getListMovieVideo(movieId: Int): StateResponse<List<VideoItemItemModel>> {
        return repository.getListMovieVideo(movieId)
    }

    override suspend fun getListMovieReviewsPaging3(
        movieId: Int,
        isNextPage: Boolean
    ): Flow<PagingData<ReviewModel>> {
        return repository.getListMovieReviewsPaging3(movieId, isNextPage = isNextPage)
    }
}
