package com.ozimos.test.module

import com.ozimos.test.data.MovieRepository
import com.ozimos.test.data.MovieRepositoryImpl
import com.ozimos.test.data.remote.ApiService
import com.ozimos.test.domain.usecase.MovieUseCase
import com.ozimos.test.domain.usecase.MovieUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    fun provideMovieRepository(repositoryImpl: MovieRepositoryImpl): MovieRepository =
        repositoryImpl

    @Provides
    fun provideMovieUseCase(usecaseImpl: MovieUseCaseImpl): MovieUseCase = usecaseImpl

}