package com.ozimos.test.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ozimos.test.data.remote.StateResponse
import com.ozimos.test.data.remote.succeeded
import com.ozimos.test.domain.model.*
import com.ozimos.test.domain.usecase.MovieUseCase
import com.ozimos.test.util.StateUi
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(private val useCase: MovieUseCase) : ViewModel() {

    private val _genre = MutableLiveData<StateUi<GenreModel>>()
    val genreResult: LiveData<StateUi<GenreModel>>
        get() = _genre

    fun getListGenre() {
        viewModelScope.launch {
            _genre.postValue(StateUi.Loading)

            val result = useCase.getListGenre()

            if (result.succeeded) {
                result as StateResponse.Success
                val data = result.data
                _genre.postValue(StateUi.Success("Success", data = data))
            } else {
                result as StateResponse.Failed
                _genre.postValue(StateUi.Failed(result.message))
            }
        }
    }

    private val _moviePaging = MutableLiveData<PagingData<MovieModel>>()
    val moviePaging: LiveData<PagingData<MovieModel>>
        get() = _moviePaging

    fun getMovieByGenrePaging(genreIds: String) {
        viewModelScope.launch {
            val result = useCase.getMovieByGenrePaging3(genreIds = genreIds).cachedIn(viewModelScope)
            result.collect {
                _moviePaging.postValue(it)
            }
        }
    }

    private val _detailMovie = MutableLiveData<StateUi<DetailMovieModel>>()
    val detailMovieResult: LiveData<StateUi<DetailMovieModel>>
        get() = _detailMovie

    fun getDetailMovie(movieId: Int) {
        viewModelScope.launch {
            _detailMovie.postValue(StateUi.Loading)

            val result = useCase.getDetailMovie(movieId = movieId)

            if (result.succeeded) {
                result as StateResponse.Success
                val data = result.data
                _detailMovie.postValue(StateUi.Success("Success", data = data))
            } else {
                result as StateResponse.Failed
                _detailMovie.postValue(StateUi.Failed(result.message))
            }
        }
    }


    private val _reviewPaging = MutableLiveData<PagingData<ReviewModel>>()
    val reviewPaging: LiveData<PagingData<ReviewModel>>
        get() = _reviewPaging

    fun getListMovieReview(movieId: Int, isNextPage: Boolean = true) {
        viewModelScope.launch {
            val result =
                useCase.getListMovieReviewsPaging3(
                    movieId = movieId,
                    isNextPage = isNextPage
                )
                    .cachedIn(viewModelScope)
            result.collect {
                _reviewPaging.postValue(it)
            }
        }
    }


    private val _movieVideos = MutableLiveData<StateUi<List<VideoItemItemModel>>>()
    val movieVideosResult: LiveData<StateUi<List<VideoItemItemModel>>>
        get() = _movieVideos

    fun getListMovieVideo(movieId: Int) {
        viewModelScope.launch {
            _movieVideos.postValue(StateUi.Loading)

            val result = useCase.getListMovieVideo(movieId = movieId)

            if (result.succeeded) {
                result as StateResponse.Success
                val data = result.data
                _movieVideos.postValue(StateUi.Success("Success", data = data))
            } else {
                result as StateResponse.Failed
                _movieVideos.postValue(StateUi.Failed(result.message))
            }
        }
    }
}
