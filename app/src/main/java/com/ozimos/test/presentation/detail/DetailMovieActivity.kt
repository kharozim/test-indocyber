package com.ozimos.test.presentation.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import coil.load
import com.google.gson.Gson
import com.ozimos.test.R
import com.ozimos.test.databinding.ActivityDetailMovieBinding
import com.ozimos.test.domain.model.DetailMovieModel
import com.ozimos.test.domain.model.MovieModel
import com.ozimos.test.domain.model.ReviewModel
import com.ozimos.test.domain.model.VideoItemItemModel
import com.ozimos.test.presentation.MovieViewModel
import com.ozimos.test.presentation.review.ReviewActivity
import com.ozimos.test.util.StateLoadingAdapterUtil
import com.ozimos.test.util.StateUi
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class DetailMovieActivity : AppCompatActivity() {

    private val binding by lazy { ActivityDetailMovieBinding.inflate(layoutInflater) }
    private val viewModel: MovieViewModel by viewModels()
    private var dataExtra: Int? = null

    private lateinit var ytubePlayer: YouTubePlayer

    @Inject
    lateinit var adapterReview: DetailReviewAdapter

    @Inject
    lateinit var adapterVideo: VideoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        getData()
        setObserver()
        setView()
    }

    private fun setView() {
        binding.run {
//            sectionVideos.rvVideo.adapter = adapterVideo
            sectionVideos.rvVideo.adapter = adapterVideo
            sectionReview.rvReview.adapter =
                adapterReview.withLoadStateFooter(footer = StateLoadingAdapterUtil {
                    adapterReview.refresh()
                })
            adapterReview.addLoadStateListener {
                sectionReview.tvAllReview.isVisible =
                    it.prepend.endOfPaginationReached && adapterReview.itemCount > 0
                sectionReview.tvEmpty.isVisible =
                    it.prepend.endOfPaginationReached && adapterReview.itemCount == 0
            }

            lifecycle.addObserver(sectionVideos.youtubePlayer)
        }
    }

    private fun setObserver() {
        detailMovieObserver()
        videoMovieObserver()
        reviewMovieObserver()
    }

    private fun detailMovieObserver() {
        viewModel.detailMovieResult.observe(this) {
            when (it) {
                is StateUi.Loading -> {
                    showLoadingDetailMovie(true)
                }
                is StateUi.Failed -> {
                    showLoadingDetailMovie(false)
                }
                is StateUi.Success -> {
                    showLoadingDetailMovie(false)
                    setDataMovie(it.data)
                }
            }
        }
    }

    private fun showLoadingDetailMovie(isLoading: Boolean) {
        binding.run {
            sectionHeader.progressGroup.isVisible = isLoading
        }
    }

    private fun setDataMovie(data: DetailMovieModel) {

        val releaseYear = data.releaseDate.split("-").first()
        val genreString = data.genres.joinToString(", ") { it.name }
        val dateString = data.releaseDate.replace("-", "/")
        val hourTime = data.runtime / 60
        val minuteTime = data.runtime - (60 * hourTime)
        val longTime = "${hourTime}h${minuteTime}m"

        binding.sectionHeader.run {
            ivPoster.load(data.posterPathClean)
            ivBackdrop.load(data.backdropPathClean)

            tvName.text = data.title
            tvTagline.text = data.tagline
            tvYear.text = getString(R.string.show_year, releaseYear)
            tvDateGenreHour.text =
                getString(R.string.show_date_genre_longtime, dateString, genreString, longTime)
            tvOverview.text = data.overview
        }

        binding.sectionReview.tvAllReview.setOnClickListener {
            val movie = MovieModel(id = data.id.toString(), title = data.title)
            val movieString = Gson().toJson(movie)
            ReviewActivity().openActivity(this, movie = movieString)
        }
    }


    private fun videoMovieObserver() {
        viewModel.movieVideosResult.observe(this) {
            when (it) {
                is StateUi.Loading -> {
                    showLoadingVideo(true)
                }
                is StateUi.Failed -> {
                    showLoadingVideo(false)
                    showIsEmptyVideo(true)
                }
                is StateUi.Success -> {
                    showLoadingVideo(false)
                    if (it.data.isEmpty()) {
                        showIsEmptyVideo(true)
                    } else {
                        showIsEmptyVideo(false)
                        setDataVideos(it.data)
                    }
                }
            }
        }
    }

    private fun showIsEmptyVideo(isEmpty: Boolean) {
        binding.sectionVideos.rvVideo.isVisible = !isEmpty
        binding.sectionVideos.tvEmpty.isVisible = isEmpty

    }

    private fun showLoadingVideo(isLoading: Boolean) {
        binding.sectionVideos.progressBar.isVisible = isLoading
    }

    private fun setDataVideos(data: List<VideoItemItemModel>) {
        binding.sectionVideos.youtubePlayer.addYouTubePlayerListener(object :
            AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                super.onReady(youTubePlayer)
                ytubePlayer = youTubePlayer
                if (data.isNotEmpty()) {
                    playVideo(data.first().key)
                }
            }
        })

        adapterVideo.setData(data)
        adapterVideo.onClick = { item ->
            playVideo(item.key)
        }

    }

    private fun playVideo(key: String) {
        if (this::ytubePlayer.isInitialized) {
            ytubePlayer.loadVideo(key, 0f)
        }
    }

    private fun reviewMovieObserver() {
        viewModel.reviewPaging.observe(this) { pagingReview ->
            setDataReview(pagingReview)
        }
    }

    private fun setDataReview(data: PagingData<ReviewModel>?) {
        lifecycleScope.launch {
            data?.let { paging ->
                adapterReview.submitData(paging)
            }
        }
    }


    private fun getData() {
        val intentExtra = intent.getIntExtra(DATA_EXTRA_MOVIE_ID, 0)
        dataExtra = intentExtra

        dataExtra?.let {
            viewModel.getDetailMovie(movieId = it)
            viewModel.getListMovieVideo(movieId = it)
            viewModel.getListMovieReview(movieId = it, isNextPage = false)
        }
    }

    fun openActivity(context: Context, movieId: Int) {
        val intent = Intent(context, DetailMovieActivity::class.java)
        intent.putExtra(DATA_EXTRA_MOVIE_ID, movieId)
        context.startActivity(intent)
    }

    override fun onDestroy() {
        lifecycle.removeObserver(binding.sectionVideos.youtubePlayer)
        super.onDestroy()
    }

    companion object {
        private const val DATA_EXTRA_MOVIE_ID = "DATA_EXTRA_MOVIE_ID"
    }
}