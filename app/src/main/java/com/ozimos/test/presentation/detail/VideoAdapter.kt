package com.ozimos.test.presentation.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.ozimos.test.databinding.ItemVideoBinding
import com.ozimos.test.domain.model.VideoItemItemModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VideoAdapter @Inject constructor() : RecyclerView.Adapter<VideoAdapter.MyViewHolder>() {

    private val items = arrayListOf<VideoItemItemModel>()

    fun setData(data: List<VideoItemItemModel>) {
        val itemCount = items.size
        items.clear()
        notifyItemRangeRemoved(0, itemCount)
        items.addAll(data)
        notifyItemRangeInserted(0, data.size)
        if (items.isNotEmpty()) {
            selectItem(items.first(), 0)
        }
    }

    private var selectedPosition = -1
    var onClick: ((VideoItemItemModel) -> Unit)? = null

    inner class MyViewHolder(private val binding: ItemVideoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(item: VideoItemItemModel, position: Int) {
            binding.run {
                tvType.text = item.type
                tvTitle.text = item.name

                ivIsSelected.isVisible = selectedPosition == position
                root.setOnClickListener {
                    selectItem(item, position)
                }
            }
        }
    }

    private fun selectItem(item: VideoItemItemModel, position: Int) {
        val oldPosition = selectedPosition
        selectedPosition = position
        if (oldPosition > -1) {
            notifyItemChanged(oldPosition)
        }
        notifyItemChanged(selectedPosition)

        onClick?.invoke(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            ItemVideoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(items[position], position)
    }

    override fun getItemCount(): Int = items.size
}