package com.ozimos.test.presentation.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ozimos.test.databinding.ItemGenreBinding
import com.ozimos.test.domain.model.GenreItemModel
import com.ozimos.test.util.DiffUtilNonPaging3

class GenreAdapter :
    RecyclerView.Adapter<GenreAdapter.MyViewHolder>() {


    private val items: ArrayList<GenreItemModel> = arrayListOf()
    private var itemsSearch: List<GenreItemModel> = emptyList()

    fun setData(data: List<GenreItemModel>) {
        items.clear()
        items.addAll(data)
        searchGenre("")
    }

    //search item
    fun searchGenre(text: String) {
        val temp = itemsSearch
        itemsSearch = items.filter { it.name.contains(text, true) }

        val diffCallback = DiffUtilNonPaging3(temp, itemsSearch)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        diffResult.dispatchUpdatesTo(this)
    }

    //set onclick item
    var onClick: ((GenreItemModel) -> Unit)? = null


    inner class MyViewHolder(private val binding: ItemGenreBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindData(item: GenreItemModel) {
            binding.tvName.text = item.name
            binding.root.setOnClickListener {
                onClick?.invoke(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            binding = ItemGenreBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(itemsSearch[position])
    }

    override fun getItemCount(): Int = itemsSearch.size
}

