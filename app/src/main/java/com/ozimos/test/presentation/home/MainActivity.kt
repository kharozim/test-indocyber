package com.ozimos.test.presentation.home

import android.content.Intent
import android.os.Bundle
import android.widget.SearchView.OnQueryTextListener
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.isVisible
import com.google.gson.Gson
import com.ozimos.test.databinding.ActivityMainBinding
import com.ozimos.test.domain.model.GenreItemModel
import com.ozimos.test.presentation.MovieViewModel
import com.ozimos.test.presentation.movie.MovieActivity
import com.ozimos.test.util.StateUi
import com.ozimos.test.util.showToast
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val viewModel: MovieViewModel by viewModels()
    private val adapter by lazy { GenreAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()

        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        getData()
        setObserver()
        setView()

    }

    private fun setView() {
        binding.run {
            searchView.setOnQueryTextListener(object : OnQueryTextListener {
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(text: String?): Boolean {
                    adapter.searchGenre(text ?: "")
                    return true
                }
            })

            layoutEmpty.btnReload.setOnClickListener { getData() }
        }
    }

    private fun setObserver() {
        viewModel.genreResult.observe(this) {
            when (it) {
                is StateUi.Loading -> {
                    showLoading(true)
                }
                is StateUi.Failed -> {
                    showLoading(false)
                    showEmptyLayout(true)
                    showToast(it.message)
                }
                is StateUi.Success -> {
                    showLoading(false)
                    setData(it.data.genres)
                }
            }

        }
    }

    private fun showEmptyLayout(isEmpty: Boolean) {
        binding.layoutEmpty.root.isVisible = isEmpty
    }

    private fun setData(genres: List<GenreItemModel>) {
        showEmptyLayout(genres.isEmpty())
        adapter.setData(genres)
        adapter.onClick = {
            val dataExtra = Gson().toJson(it)
            val intent = Intent(this, MovieActivity::class.java)
            intent.putExtra(MovieActivity.DATA_EXTRA_GENRE, dataExtra)
            startActivity(intent)
        }


        binding.rvGenre.adapter = adapter
    }


    private fun showLoading(isLoading: Boolean) {
        binding.run {
            progressBar.isVisible = isLoading
        }
    }

    private fun getData() {
        viewModel.getListGenre()
    }

}