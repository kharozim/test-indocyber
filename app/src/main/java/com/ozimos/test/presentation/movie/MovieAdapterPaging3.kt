package com.ozimos.test.presentation.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.ozimos.test.databinding.ItemMovieBinding
import com.ozimos.test.domain.model.MovieModel
import com.ozimos.test.util.DiffUtilPaging3
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieAdapterPaging3 @Inject constructor() :
    PagingDataAdapter<MovieModel, MovieAdapterPaging3.MyViewHolder>(DiffUtilPaging3()) {

    var onclick: ((MovieModel) -> Unit)? = null

    inner class MyViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(item: MovieModel) {
            binding.run {
                ivImage.load(item.posterPathClean)
                tvName.text = item.title

                root.setOnClickListener { onclick?.invoke(item) }
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        getItem(position)?.let { holder.bindData(it) }
    }

}
