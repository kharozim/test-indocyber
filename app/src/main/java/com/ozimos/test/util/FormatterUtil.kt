package com.ozimos.test.util

import java.text.SimpleDateFormat
import java.util.*

object FormatterUtil {

    fun stringToDate(
        date: String,
        formatFrom: String = "yyyy-MM-dd",
    ): Date {
        val formatter = SimpleDateFormat(formatFrom, Locale.getDefault())
        return formatter.parse(date)
    }

    fun dateToString(date: Date, formatTo: String = "yyyy-MM-dd HH:mm"): String {
        val simpleDateFormat = SimpleDateFormat(formatTo)
        return simpleDateFormat.format(date)
    }
}
