package com.ozimos.test.util

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ozimos.test.databinding.LoadingStateBinding

class StateLoadingAdapterUtil(private val onclick: () -> Unit) :
    LoadStateAdapter<StateLoadingAdapterUtil.MyViewHolder>() {

    inner class MyViewHolder(private val binding: LoadingStateBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(loadState: LoadState) {
            binding.run {
                Log.e("TAG", "loadingState: $loadState" )
                tvNote.isVisible = loadState is LoadState.Error || loadState is LoadState.NotLoading
                btnRetry.isVisible = loadState is LoadState.Error
                progressCircular.isVisible = loadState is LoadState.Loading

                if(loadState is LoadState.Error){
                    tvNote.text = loadState.error.message ?: "Failed load data"
                    btnRetry.setOnClickListener { onclick.invoke() }
                }

            }
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder, loadState: LoadState) {
        holder.bindData(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): MyViewHolder {
        return MyViewHolder(
            LoadingStateBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }
}