package com.ozimos.test.util
sealed class StateUi<out T : Any> {
    object Loading : StateUi<Nothing>()
    data class Success<out S : Any>(val message: String, val data: S) : StateUi<S>()
    data class Failed(val message: String) : StateUi<Nothing>()
}
